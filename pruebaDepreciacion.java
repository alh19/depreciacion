package pruebadepreciacion;

import java.util.Scanner;
/**
 *
 * @author alh19
 */
public class PruebaDepreciacion {
    
    static Scanner entrada = new Scanner(System.in);
    
        public static void main(String[] args) {
            
            int ano = 0;
            double numAnos, valorInicial;
            double depreciacion, valorActual;
            
            System.out.println("Calculo de la depreciacion anual de un valor");
            System.out.print("Introduce el número de años: ");
            numAnos = entrada.nextInt();
            System.out.print("Introduce el valor inicial: ");
            valorInicial = entrada.nextInt();
            System.out.println("");
            
            final double denominador = PruebaDepreciacion.calcularDenominador(numAnos);
            while(numAnos!=0){
                ano=ano+1;
                depreciacion=valorInicial*(numAnos/denominador);
                valorActual=valorInicial-depreciacion;
                numAnos--;
                System.out.print("Final del año: "+ano);
                System.out.printf("    Depreciacion %.2f", depreciacion);
                System.out.printf("    Valor Actual %.2f %n", valorActual);
            }
        }
        
        public static double calcularDenominador(double numAnos){
            double denominador = 0;
            while(numAnos!=0){
                denominador=denominador+numAnos;
                numAnos--;
            }
            return denominador;
            }
}
